﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WizardMvc.Models
{
    [Serializable]
    public class Step3Model
    {
        [Required]
        public string Seven { get; set; }
        public string Eight { get; set; }
        public string Nine { get; set; }
    }
}