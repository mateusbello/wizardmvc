﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WizardMvc.Models
{
    [Serializable]
    public class Step1Model
    {
        [Required]
        public string One { get; set; }
        public string Two { get; set; }
        public string Three { get; set; }
    }
}