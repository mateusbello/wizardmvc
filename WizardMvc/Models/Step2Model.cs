﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WizardMvc.Models
{
    [Serializable]
    public class Step2Model
    {
        [Required]
        public string Four { get; set; }
        public string Five { get; set; }
        public string Six { get; set; }
    }
}