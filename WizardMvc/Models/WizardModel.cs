﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WizardMvc.Models
{
    [Serializable]
    public class WizardModel
    {
        public WizardModel()
        {
            Step1 = new Step1Model();
            Step2 = new Step2Model();
            Step3 = new Step3Model();
        }

        public int CurrentStepIndex { get; set; }
        public Step1Model Step1 { get; set; }
        public Step2Model Step2 { get; set; }
        public Step3Model Step3 { get; set; }

    }

   
}