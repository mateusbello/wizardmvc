﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WizardMvc.Models;

namespace WizardMvc.Controllers
{
    public class WizardController : Controller
    {

        public ActionResult Index()
        {
            var wizard = new WizardModel();
            return View(wizard);
        }

        [HttpPost]
        public ActionResult Step1(Step1Model step)
        {
            var wizard = SerializationUtility.Deserialize(Request.Form["wizard"]) as WizardModel;
            wizard.Step1 = step;
            return GetWizardStep(wizard);
        }

        [HttpPost]
        public ActionResult Step2(Step2Model step)
        {
            var wizard = SerializationUtility.Deserialize(Request.Form["wizard"]) as WizardModel;
            wizard.Step2 = step;
            return GetWizardStep(wizard);
        }

        [HttpPost]
        public ActionResult Step3(Step3Model step)
        {
            var wizard = SerializationUtility.Deserialize(Request.Form["wizard"]) as WizardModel;
            wizard.Step3 = step;
            return GetWizardStep(wizard);
        }

        private ActionResult GetWizardStep(WizardModel wizard)
        {
            if (ModelState.IsValid)
            {
                if (!string.IsNullOrEmpty(Request.Form["next"]) && Request.Form["next"] == "Next")
                {
                    wizard.CurrentStepIndex++;
                }
                else if (!string.IsNullOrEmpty(Request.Form["prev"]) && Request.Form["prev"] == "Previous")
                {
                    wizard.CurrentStepIndex--;
                }
                else
                {
                    // TODO: we have finished: all the step partial
                    // view models have passed validation => map them
                    // back to the domain model and do some processing with
                    // the results

                    return Content("thanks for filling this form", "text/plain");
                }
            }
            else if (!string.IsNullOrEmpty(Request.Form["prev"]) && Request.Form["prev"] == "Previous")
            {
                // Even if validation failed we allow the user to
                // navigate to previous steps
                wizard.CurrentStepIndex--;
            }
            return View("Index", wizard);
        }
    }

}