﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WizardMvc.Models;

namespace WizardMvc.Controllers
{
    public class WizardAjaxController : Controller
    {

        [ChildActionOnly]
        public ActionResult Index()
        {
            var wizard = new WizardModel();
            return PartialView(wizard);
        }

        [HttpPost]
        public ActionResult Step1(Step1Model step1, string action)
        {
            var wizard = SerializationUtility.Deserialize(Request.Form["wizard"]) as WizardModel;
            wizard.Step1 = step1;
            return GetWizardStep(wizard, action);
        }

        [HttpPost]
        public ActionResult Step2(Step2Model step2, string action)
        {
            var wizard = SerializationUtility.Deserialize(Request.Form["wizard"]) as WizardModel;
            wizard.Step2 = step2;
            return GetWizardStep(wizard, action);
        }

        [HttpPost]
        public ActionResult Step3(Step3Model step3, string action)
        {
            var wizard = SerializationUtility.Deserialize(Request.Form["wizard"]) as WizardModel;
            wizard.Step3 = step3;
            return GetWizardStep(wizard, action);
        }

        private ActionResult GetWizardStep(WizardModel wizard, string action)
        {
            if (ModelState.IsValid)
            {
                if (!string.IsNullOrEmpty(action) && action == "Next")
                {
                    wizard.CurrentStepIndex++;
                }
                else if (!string.IsNullOrEmpty(action) && action == "Previous")
                {
                    wizard.CurrentStepIndex--;
                }
                else
                {
                    // TODO: we have finished: all the step partial
                    // view models have passed validation => map them
                    // back to the domain model and do some processing with
                    // the results

                    return Content("thanks for filling this form", "text/plain");
                }
            }
            else if (!string.IsNullOrEmpty(action) && action == "Previous")
            {
                // Even if validation failed we allow the user to
                // navigate to previous steps
                wizard.CurrentStepIndex--;
            }
            return PartialView("Index", wizard);
        }
    }

}